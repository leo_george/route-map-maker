import { UPDATE_MAP_DATA } from "./action-type";

const initialState = {
  mapData: [],
};

export const rootReducer = (state = initialState, action) => {
  let newState;
  switch (action.type) {
    case UPDATE_MAP_DATA:
      newState = {
        mapData: action.payload,
      };
      return newState;
    default:
      return state;
  }
};
