import React from 'react';
import SendMessage from './components/SendMeaage';
import ReceiveMessage from './components/ReceiveMessage';
function App() {
  return (
    <div className="flex h-screen">
    <div className="w-1/4 h-12"><SendMessage/></div>
    <div className="w-3/4 h-12"><ReceiveMessage/></div>
  </div>
  );
}

export default App;
