import React from "react";
import { useSelector } from "react-redux";
import { Map, Marker, Popup, TileLayer } from "react-leaflet";
import "./DisplayMap.css";
const DisplayMap = () => {
  const mapData = useSelector((state) => state.mapData);
  return (
    <Map center={[10.54319, 76.136627]} zoom={10}>
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      />
      {mapData.map((data, idx) => (
        <Marker key={`marker-${idx}`} position={[data.lat, data.lon]}>
          <Popup>
            <span>
              {data.place} <br /> {data.date.toString()}
            </span>
          </Popup>
        </Marker>
      ))}
    </Map>
  );
};

export default DisplayMap;
