import React from "react";
import { useSelector } from "react-redux";
import no_data from "./no_data.svg"
const DisplayTimeline = () => {
  const mapData = useSelector((state) => state.mapData);
  return (
    <>
        {mapData.length === 0?
            <div className="flex flex-col items-center">
                <img src={no_data} alt="no data" width="40%"></img>
                <p>No Data</p>
            </div>    
        :''}
        <div className="history-tl-container">
        <ul className="tl">
            {mapData.map((item, index) => (
            <li key={item + "_" + index} className="tl-item">
                <div className="timestamp">
                {item.date.split(",")[0]}
                <br />
                {item.date.split(",")[1]}
                </div>
                <div class="item-title">{item.place}</div>
                <div class="item-detail">
                {item.lat},{item.lon}
                </div>
            </li>
            ))}
        </ul>
        </div>
    </>
  );
};

export default DisplayTimeline;
