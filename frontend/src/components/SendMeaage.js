import React, { useState } from "react";
import { sendMessageData, getDetails } from "../api";

const SendMessage = () => {
  const [phno, setPhno] = useState("");
  const [pcount, setPatientsCount] = useState(0);
  const [message, setMessage] = useState("");
  const formData = (e) => {
    e.preventDefault();
    if (phno !== "" && message !== "") {
      const data = {
        phno: phno,
        message: message,
        pcount : pcount
      };
      // if (message === "Get Data") {
      //   getDetails(data);
      // } else {
      sendMessageData(data);
      alert("message send");
      // }
    }
  };
  return (
    <div className="h-screen py-5">
      <form
        className="h-full flex justify-between flex-col"
        onSubmit={formData}
      >
        <div>
          <h3 className=" px-3 font-semibold text-2xl pb-5">Send Message</h3>
          <div className="px-3 mb-6">
            <label className="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2">
              Phone number
            </label>
            <input
              className="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4"
              type="text"
              placeholder="9999 999 999"
              onChange={(e) => setPhno(e.target.value)}
            />
          </div>
          <div className="px-3">
            <label className="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2">
              Position
            </label>
            <div className="relative">
              <select
                className="block appearance-none w-full bg-grey-lighter border border-grey-lighter text-grey-darker py-3 px-4 pr-8 rounded"
                onChange={(e) => setMessage(e.target.value)}
              >
                <option></option>
                <option>Positive</option>
                <option>Negative</option>
                <option>Recovered</option>
                <option>False Positive</option>
                <option>Get Data</option>
              </select>
            </div>
          </div>
        </div>
        {
        (()=>{
          if (message ===  "Positive" || message === "Negative" || message === "Recovered") {
            return  <div className="px-3 mb-6 mt-6">
            <label className="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2">
              Patient Count
            </label>
            <input
              className="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4"
              type="text"
              placeholder="Number of Patients"
              onChange={(e) => setPatientsCount(e.target.value)} value = {pcount}
            />
          </div>
          }
        })()
        }
        <div className="flex justify-center">
          <button
            type="submit"
            className={
              "bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" +
              (phno === "" || message === ""
                ? " opacity-50 cursor-not-allowed"
                : "")
            }
          >
            Send Message
          </button>
        </div>
      </form>
    </div>
  );
};

export default SendMessage;
