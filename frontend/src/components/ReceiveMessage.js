import React, { useState } from "react";
import DisplayMap from "./DisplayMap";
import DisplayTimeline from "./DisplayTimeline";

const ReceiveMessage = () => {
  const [active, setActive] = useState("map");
  const tabItems = ["Map","Timeline"]
  return (
    <div className="flex flex-col pt-4">
      <div className="flex">
        <ul className="flex cursor-pointer">
          {tabItems.map(item =>(
            <li
              key={item}
              className={"py-2 px-6 bg-white rounded-t-lg" +(active === item.toLowerCase() ? " text-gray-500 bg-gray-200" : "")}
              onClick={() => setActive(item.toLowerCase())}
            >
              {item}
            </li>
          ))}
        </ul>
      </div>
      {active === "map" ? <DisplayMap /> : <DisplayTimeline />}
    </div>
  );
};

export default ReceiveMessage;
