import  {UPDATE_MAP_DATA} from './action-type';
 
export const updateMapData = (payload) =>(
    {
        type:UPDATE_MAP_DATA,
        payload
    }
);