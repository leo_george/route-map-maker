\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{List of Figures}{v}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{List of Tables}{vi}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Overview}{1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Problem Statement}{3}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Objective}{3}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Related works}{4}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Arogya Setu}{4}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}OHIOH App}{4}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Trace Together}{5}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Design}{6}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Architecture}{6}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.1}Internal Architectures}{7}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Admin Panel Architecture}{7}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Route Map App Architecture}{7}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Data Flow Diagrams}{8}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}DFD Level 0}{8}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}DFD Level 1}{9}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Implementation}{9}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Nodejs server}{9}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Bootstrap Module}{9}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Communication Module}{10}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Admin Panel}{10}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{User Interface}{10}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.3}Android Application}{11}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{User authentication}{11}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Map Fragment}{11}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Location History Fragment}{11}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Interval Service}{11}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{RoomDB}{12}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Libraries}{12}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.1}Nodejs server}{12}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.2}Admin panel}{12}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.3}Android Application}{12}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Results}{13}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Comparison With Existing Systems}{15}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Conclusion}{16}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Relevance of Project}{16}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Future Enhancements}{16}% 
