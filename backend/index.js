var express = require('express');
var app = express();
var path = require('path');
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;
const PgHelper = require('./db-helper');


const pgHelper = new PgHelper()
const pgcon = pgHelper.connectNow(); 


const {
    ON_LOGIN,
    LOGIN_OUTCOME,
    ON_BLOCK_RECEIVED,
    BLOCK_DETAILS
} = require("./constants")

server.listen(port,()=>{
    console.log(`Server is running at port:${port}`);
   
    
})


// Public Dirs
app.use(express.static(path.join(__dirname,'public')));





io.on('connection',(socket)=>{

    console.log(`[${Date()}] user_id :${socket.id} connected `);
    

    socket.on('onmessage',(data)=>{
        
        socket.broadcast.emit('onmessage',{
            imei : data
        })
    })

    socket.on(ON_LOGIN,(data)=>{
        console.log(`${socket.id},${data}`);
        
        const {phno}  = JSON.parse(data);
        
        pgcon.then(()=>{        
            return pgHelper.makeQuery(`select * from registration.reg where phno='${phno}'`);
        })
        .then((queryResult)=>{
            
            if(!queryResult.rows.length){
                pgHelper.makeQuery(`INSERT INTO registration.reg(phno)
                    VALUES (${phno});`)

                    socket.emit(LOGIN_OUTCOME,
                    JSON.stringify({ status:"OK",msg:`${phno} is registered now`}))
                    
                    // console.log("You are registered now");
                    
            }
            else
            {
                socket.emit(LOGIN_OUTCOME,JSON.stringify({ status:"OK",msg:`Welcome back ${phno}`}))
                // console.log("Welcome back");
                
            }
        })        
    });

    socket.on(BLOCK_DETAILS,(data)=>{
        console.log(data);
        socket.broadcast.emit(ON_BLOCK_RECEIVED,data)
    })
    socket.on('sendBroadcast',(data)=>{
        console.log(data);
        socket.broadcast.emit('onCommandBroadcast',data)
    })
    socket.on('requestDetails',(data)=>{
        console.log(data);
        const dummyData =[
            {
                place:"ende stlalam",
                "date": new Date().toLocaleString(),
                lat:11.54,
                long:79
            },
            {
                place:"ende stlalam 2",
                "date": new Date().toLocaleString(),
                lat:8.3,
                long:72.98
            },
            {
                place:"ende stlalam OP",
                "date": new Date().toLocaleString(),
                lat:10.54,
                long:76.10
            }
        ];
        io.emit('getDetails',data)

    })

})