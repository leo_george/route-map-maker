module.exports = {
    ON_LOGIN : "onlogin",
    LOGIN_OUTCOME : "loginoutcome",
    BLOCK_DETAILS : "blockdetails",
    ON_BLOCK_RECEIVED : "onBlockReceived"
}