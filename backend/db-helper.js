const {Client} = require('pg')

module.exports = class PgHelper{
    

    constructor(){
        this.client = new Client("postgres://leo@localhost:5432/final")
    }


    async connectNow(){ 
       return await this.client.connect()
    };
    
    makeQuery (query) {
        return this.client.query(query);
    }
}

