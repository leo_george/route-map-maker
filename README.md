# Route map creator 
 A decentralized application to create route map of individuals during a epidemic or pandemic

 ## Prerequisites

The project consists of a NodeJs server, ReactJs (Admin panel) and an Android App.
We assume the following tools are installed.
1. nodejs
    For Debain based systems
    ```
    sudo apt install nodejs
    ```

    For arch based systems
    ```
    sudo pacman -Syu nodejs
    ```
2. Android studio 
   Required to compile and build apk's which can be installed to android phones.[Official Guide](https://developer.android.com/studio/install)

## Setting Up Your Environment

### Admin panel

```
cd ./frontend
npm install
```
### Bootstrap server
```
cd ./backend
npm install
```
### Android App
1. Open Android studio and import the project
2. Sync the project

## Running
1. Run the node server
    ```
    cd ./backend
    npm start
    ```
2. Run the Admin panel
    ```
    cd ./frontend
    npm start
    ```
3. Running android App
   edit `SERVER_URL` in `./app/src/main/java/SIO.kt` to the ip of the machine running the node server.  

   Then build and run using android studio.
