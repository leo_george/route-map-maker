package com.leo.kotlinuibackend

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer


class SearchFieldView(
    private val lifecycleOwner: LifecycleOwner,
    private val transactionRecyclerAdapter: TransactionRecyclerAdapter,
    private val transactionViewModel: TransactionViewModel
    ):TextWatcher{

    override fun afterTextChanged(s: Editable?) {}

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        if (s!!.isNotEmpty()) {
            transactionViewModel.allTransactions.observe(lifecycleOwner, Observer {
                items -> items?.let { transactionRecyclerAdapter.doFilteringWithChoice(s.toString(),it) }
            })
        }
        else{
            transactionViewModel.allTransactions.observe(lifecycleOwner, Observer {
                    items -> items?.let { transactionRecyclerAdapter.updateTransactions(it) }
            })
        }
    }

}