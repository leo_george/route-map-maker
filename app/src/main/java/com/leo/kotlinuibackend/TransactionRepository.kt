package com.leo.kotlinuibackend

import androidx.lifecycle.LiveData

class TransactionRepository(private val transactionDao: TransactionDao) {
    val allTransactions:LiveData<List<TransactionModel>> =transactionDao.getAllTransactions()

   fun insert(transaction: TransactionEntity){
        transactionDao.newTransaction(transaction)
    }
}