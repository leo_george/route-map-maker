package com.leo.kotlinuibackend

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface TransactionDao {
    @Query("select * from transactions order by id DESC")
    fun getAllTransactions():LiveData<List<TransactionModel>>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun newTransaction(transaction: TransactionEntity)

    @Query("delete from transactions")
    fun deleteAllTransactions()

}