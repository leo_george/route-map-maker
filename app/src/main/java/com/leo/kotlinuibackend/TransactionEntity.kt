package com.leo.kotlinuibackend

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "transactions")
data class TransactionEntity (
    @ColumnInfo(name = "place") val place:String,
    @ColumnInfo(name = "lat") val lat:String,
    @ColumnInfo(name = "lon") val lon: String,
    @ColumnInfo(name = "date") val date: String = Date().toGMTString(),
    @PrimaryKey(autoGenerate = true) val id:Long? = null
)