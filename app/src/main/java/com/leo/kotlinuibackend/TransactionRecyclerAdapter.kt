package com.leo.kotlinuibackend

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Spinner
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class TransactionRecyclerAdapter internal constructor(
    context: Context,
    private val choiceSpinner: Spinner
    ):RecyclerView.Adapter<TransactionRecyclerAdapter.TransactionViewHolder>(){

    private val inflater = LayoutInflater.from(context)
    private var transactions = emptyList<TransactionModel>()

    private val TAG = this.javaClass.simpleName

    inner class TransactionViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        val placeName:TextView = itemView.findViewById(R.id.place_name)
        val latitude:TextView = itemView.findViewById(R.id.place_lat)
        val longitude:TextView = itemView.findViewById(R.id.place_long)
        val date:TextView =itemView.findViewById(R.id.date)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
    val itemView = inflater.inflate(R.layout.recyclerview_item,parent,false)
        return TransactionViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        val current = transactions[position]
        holder.placeName.text =current.place
        holder.latitude.text = "lat: ${current.lat}"
        holder.longitude.text ="lon: ${current.lon}"
        holder.date.text = current.date
    }

    override fun getItemCount(): Int = transactions.size

    internal fun doFilteringWithChoice(searchString: String,originalList: List<TransactionModel>){
        val temp = mutableListOf<TransactionModel>()
        if(searchString.isNotEmpty()) {
            when(choiceSpinner.selectedItemPosition){
                    Choices.ADDRESS.ordinal -> originalList.forEach{if (it.place.contains(searchString,true)) temp.add(it)}
                    Choices.LATITUDE.ordinal -> originalList.forEach{if (it.lat.contains(searchString,true)) temp.add(it)}
                    Choices.LONGITUDE.ordinal -> originalList.forEach{if (it.lon.contains(searchString,true)) temp.add(it)}
                    Choices.DATE.ordinal -> originalList.forEach{if (it.date.contains(searchString,true)) temp.add(it)}
                }

            this.transactions = temp
            notifyDataSetChanged()
        }
    }
    internal fun updateTransactions(transactions: List<TransactionModel>){
        this.transactions=transactions
        notifyDataSetChanged()
    }

}