package com.leo.kotlinuibackend

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TransactionViewModel (application: Application):AndroidViewModel(application){
    val transactionDao=ChainRoomDatabase.getDataBaseInstance(application,viewModelScope).getTransactionDao()
    val repository=TransactionRepository(transactionDao)
    val allTransactions =repository.allTransactions

    fun insert(transactionModel: TransactionEntity)=viewModelScope.launch(Dispatchers.IO) {
     repository.insert(transactionModel)
    }
}