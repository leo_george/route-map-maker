package com.leo.kotlinuibackend

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_location_history.*
import kotlinx.android.synthetic.main.search_tile.*

class LocationHistoryFragment : Fragment() {
    private lateinit var context1: Context

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_location_history, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val transactionAdapter = TransactionRecyclerAdapter(context!!, spinner_choice)
        val recyclerView = recyclerview_transactions
        recyclerView.adapter = transactionAdapter
        recyclerView.layoutManager = LinearLayoutManager(context)

        val transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel::class.java)
        transactionViewModel.allTransactions.removeObservers(this)
        transactionViewModel.allTransactions.observe(this, Observer { items ->
            items?.let { transactionAdapter.updateTransactions(it) }
        })

        spinner_choice.adapter = SearchChoiceAdapter.getAdapter(context!!)
        search_box.addTextChangedListener(
            SearchFieldView(
                this,
                transactionAdapter,
                transactionViewModel
            )
        )

    }


}