package com.leo.kotlinuibackend


enum class Choices(index:Int){
    ADDRESS(0),
    LATITUDE(1),
    LONGITUDE(2),
    DATE(3);

    object Util{
        fun getStringList():List<String>{
            return values().map { choices -> choices.name.toLowerCase().capitalize() }
        }
    }
}