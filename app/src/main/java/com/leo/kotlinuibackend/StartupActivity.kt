package com.leo.kotlinuibackend

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class StartupActivity : AppCompatActivity() {
    companion object {
        const val AUTHENTICATION_CODE = 1524
        const val SHARED_PREF = "SHARED_PREF"
        const val LOGGED_IN = "LOGGED_IN"
        const val PH_NO = "PH_NO"

    }

    override fun onCreate(savedInstanceState: Bundle?) {

        if (isLoggedIn()) {
            startMainActivity()
            finish()
        } else
            startActivityForResult(
                Intent(this, LoginActivity::class.java),
                AUTHENTICATION_CODE
            )

        super.onCreate(savedInstanceState)
    }

    private fun isLoggedIn():Boolean{
    return getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE).getBoolean(LOGGED_IN, false)
    }

    private fun startMainActivity() {
        lateinit var intentToActivity:Intent

        if (Utils.didShowForFirstTime(this)) {
             intentToActivity = Intent(this, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or
                        Intent.FLAG_ACTIVITY_NEW_TASK or
                        Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
        }
        else {
            intentToActivity = Intent(this, ExternalStoragePermissionActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or
                        Intent.FLAG_ACTIVITY_NEW_TASK or
                        Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
        }
        startActivity(intentToActivity)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTHENTICATION_CODE && resultCode == Activity.RESULT_OK) {
            startMainActivity()
        }
        finish()
    }
}