package com.leo.kotlinuibackend

import android.app.Activity
import android.content.Context
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.edit
import com.github.nkzawa.emitter.Emitter
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject


class LoginActivity : AppCompatActivity() {
    private val  sio = SIO.getInstance()

    private val tag = this::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val roboto = Typeface.createFromAsset(assets, "font/roboto/Roboto-Light.ttf")
        greetings.typeface = roboto

        sio.doConnect()

        sio.listenToLoginOutcome(Emitter.Listener {
            Log.d(tag,it.joinToString())
            val loginOutcome = JSONObject(it.joinToString())
            if(loginOutcome.getString("status") == "OK"){

                setLoggedIn(true)
                setPhoneNo(phBox.text.toString())
                runOnUiThread {
                    Toast.makeText(this
                        ,loginOutcome.getString("msg"),Toast.LENGTH_LONG).show()
                }

                setResult(Activity.RESULT_OK)
                finish()
            }
        })


        login_success_button.setOnClickListener {
            if (!isNumberValid()) {
                showSnackbar("Enter a valid number")
            } else if (!isChecked()) {
                showSnackbar("Check the license agreement")
            } else {
                if (isOnline()) {
                    val json = JSONObject()
                    json.put("phno",phBox.text.toString())
                    sio.emitLoginDetails(json.toString(2))
                } else {
                    showSnackbar("You aren't online")
                }
            }
        }


    }

    private fun showSnackbar(message: String) {
        currentFocus?.let {
            Snackbar.make(it, message, Snackbar.LENGTH_SHORT)
                .apply {
                    setAction("OK", null)
                    show()
                }
        }
    }

    private fun setLoggedIn(value: Boolean) {
        return getSharedPreferences(
            StartupActivity.SHARED_PREF,
            Context.MODE_PRIVATE
        ).edit { putBoolean(StartupActivity.LOGGED_IN, value) }
    }
    private fun setPhoneNo(value:String ) {
        return getSharedPreferences(
            StartupActivity.SHARED_PREF,
            Context.MODE_PRIVATE
        ).edit { putString(StartupActivity.PH_NO,value) }
    }
    private fun isNumberValid(): Boolean {
        return phBox.text.length == 10
    }

    private fun isChecked(): Boolean {
        return agreementBox.isChecked
    }

    private fun isOnline(): Boolean {
        return (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
            .activeNetworkInfo?.isConnectedOrConnecting ?: false

    }

//    private fun publicIp():String{
//        return runBlocking (Dispatchers.IO){  URL("http://bot.whatismyipaddress.com/").readText().trim()}
//    }
//
//    private fun registerToFireStore(
//        imei: String,
//        data: HashMap<String, Any>,
//        result: FirebaseResult
//    ) {
//        Firebase.firestore
//            .collection(COLLECTION_ID)
//            .document(imei)
//            .set(data)
//            .addOnSuccessListener { result.getResult(true) }
//            .addOnFailureListener { result.getResult(false) }
//    }
//
//    private fun checkIfAlreadyRegistered(imei: String, result: FirebaseResult) {
//        Firebase.firestore.collection(COLLECTION_ID).document(imei)
//            .get()
//            .addOnSuccessListener {
//                if(it.data.isNullOrEmpty())
//                    result.getResult(false)
//                else
//                    result.getResult(true)
//            }
//            .addOnFailureListener { result.getResult(false) }
//    }


    companion object {
        private const val COLLECTION_ID = "uniqueid"
    }
}