package com.leo.kotlinuibackend

import android.app.Activity
import android.content.Context
import android.os.AsyncTask
import android.util.Log.d
import com.google.android.gms.location.LocationServices
import com.google.gson.GsonBuilder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers


class IntervalTask : AsyncTask<Void, Void, Void>() {
    private lateinit var blockModel: BlockModel

    companion object {

        private const val TAG = "Interval Task"
        private lateinit var context: Context
        private lateinit var sio: SIO

        fun startAsyncTask(context: Context,sio:SIO): AsyncTask<Void, Void, Void> {
            this.context = context
            this.sio = sio

            return IntervalTask().execute()
        }
    }

    override fun doInBackground(vararg params: Void?): Void? {
        val transactionDao = ChainRoomDatabase.getDataBaseInstance(
            IntervalService.context!!, CoroutineScope(
                Dispatchers.IO
            )
        )
            .getTransactionDao()
        val transactionRepository = TransactionRepository(transactionDao)
        val locationInterface = LocationInterface.getInstance()
        val fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(
            IntervalService.context!!
        )

        var transactionCounter = 0
        while (true) {
            d(
                TAG,
                "Permission Granted " + locationInterface.checkPermission(IntervalService.context!!)
            )
            if (locationInterface.checkPermission(IntervalService.context!!)) {

                locationInterface.getLastKnownLocation(
                    IntervalService.context!!,
                    fusedLocationProviderClient
                )
                locationInterface.lastAddress?.let {

                    val transactionEntity = TransactionEntity(
                        it,
                        locationInterface.latitude.toString(),
                        locationInterface.longitude.toString()
                    )
                    val transactionModel = TransactionModel(
                        it,
                        locationInterface.latitude.toString(),
                        locationInterface.longitude.toString()
                    )
                    val sha256 = Utils.getSHA256Hash(transactionModel)

                    when (transactionCounter) {
                        0 -> {
                            blockModel =
                                Utils.getPhoneNo(context)?.let {phno->
                                    BlockModel.create(phno)
                                }!!
                            d(TAG,transactionCounter.toString())
                            transactionCounter++
                        }
                        in 1 until BlockModel.BLOCK_SIZE -> {
                            blockModel.addTransaction(sha256 to transactionModel)
                            transactionCounter++
                            d(TAG,transactionCounter.toString())
                        }
                        BlockModel.BLOCK_SIZE -> {
                            transactionCounter = 0
                            val gson = GsonBuilder().run {
                                create().run {
                                    toJson(blockModel)
                                }
                            }
                            d(TAG,transactionCounter.toString())
                            d(TAG,gson)

                            sio.emitBlockDetails(gson)

                        }
                    }

                    transactionRepository.insert(transactionEntity)
                }

                Thread.sleep(IntervalService.INTERVAL_TIME)
            } else {
                locationInterface.requestPermission(IntervalService.context as Activity)
            }
        }
    }

}