package com.leo.kotlinuibackend

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.os.IBinder
import android.util.Log
import android.util.Log.d
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProviders
import com.github.nkzawa.emitter.Emitter
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.*
import org.json.JSONObject
import java.util.*
import java.util.logging.Handler
import kotlin.concurrent.fixedRateTimer
import kotlin.concurrent.thread
import kotlin.concurrent.timerTask


class IntervalService : Service(){
    val TAG = IntervalService::class.java.simpleName

    companion object{
        const val CHANNEL_ID = "10003"
        const val NOTIFICATION_TITLE = "DApp"
        const val NOTIFICATION_TEXT = "Running in background"
        const val FOREGROUND_ID = 1000
        const val INTERVAL_TIME = 10000.toLong()
        var context:Context? =null

        fun startService(context: Context){
            val statIntent = Intent(context,IntervalService::class.java)
            ContextCompat.startForegroundService(context,statIntent)
            this.context=context
        }

        fun stopService(context: Context){
            val stopIntent = Intent(context,IntervalService::class.java)
            context.stopService(stopIntent)
        }
    }



    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        createNotificationChannel()
        val notificationIntent = Intent(
            this,MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivities(this,0, arrayOf(notificationIntent),0)
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(NOTIFICATION_TITLE)
            .setContentText(NOTIFICATION_TEXT)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(pendingIntent)
            .build()

        startForeground(FOREGROUND_ID,notification)
        val sio = SIO.getInstance()
        sio.listenToBlockDetails(Emitter.Listener {
            d(TAG,it.joinToString())
            Utils.jsonToExternalStorage(it.joinToString(), context!!)
        })
        sio.listenToCommandReceive(Emitter.Listener {
            val incomingJson = JSONObject(it.joinToString())
            d(TAG,it.joinToString())
            when(incomingJson.getString("message")){
                "Positive" ->{
                    d(TAG,"positive")
                    createNotificationChannel("887")
                    val np = NotificationCompat.Builder(this, "887")
                        .setContentTitle("Careful ...")
                        .setContentText("${incomingJson.getString("pcount")} positive cases today")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setOngoing(false)
                        .build()
                    startForeground(887,np)
                }
                "Negative" -> {
                    d(TAG,"negative")
                    createNotificationChannel("888")
                    val nn=NotificationCompat.Builder(this, "888")
                        .setContentTitle("Relief day for kerala")
                        .setContentText("${incomingJson.getString("pcount")} negative cases today")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setOngoing(false)
                        .build()
                    startForeground(888,nn)

                }
                "Recovered" ->  {
                    d(TAG,"recovered")
                    createNotificationChannel("6868")
                    val nr=NotificationCompat.Builder(this, "6868")
                        .setContentTitle("Recoveries today")
                        .setContentText("${incomingJson.getString("pcount")} patients recovered")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setOngoing(false)
                        .build()
                    startForeground(6868,nr)
                }
                "Get Data" ->  {
                    var oneTimer = false
                    d(TAG,"data retireval")
                    d(TAG,Utils.getPhoneNo(context!!)!!)
                    if (incomingJson.getString("phno")==  Utils.getPhoneNo(context!!) ){

                        //Take the activity in the phone this wont work in background
                        GlobalScope.launch (Dispatchers.Main) {
                            val roomObserver = ChainRoomDatabase.getDataBaseInstance(context!!, this)
                                .getTransactionDao()
                                .getAllTransactions()
                                .observe(context as AppCompatActivity, androidx.lifecycle.Observer {
                                    d(TAG, it.toString())
                                    val gson = Gson()
                                    val json = gson.toJson(it)
                                    d(TAG, json)
                                    sio.emitTransDetails(json)
                                })
                        }

                    }
                }
                "False Positive"-> d(TAG,"false positive")


            }
        })
        IntervalTask.startAsyncTask(context!!,sio)
        return START_NOT_STICKY
    }
    private fun createNotificationChannel(channelId:String = CHANNEL_ID):NotificationManager?{
        var notificationManager: NotificationManager? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val serviceChannel = NotificationChannel(channelId,
                "IntervalService",
                NotificationManager.IMPORTANCE_DEFAULT)

            notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(serviceChannel)
        }
        return notificationManager
    }


}
