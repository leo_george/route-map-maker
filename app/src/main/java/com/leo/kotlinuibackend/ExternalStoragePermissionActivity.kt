package com.leo.kotlinuibackend

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import kotlinx.android.synthetic.main.activity_extern_permission.*

class ExternalStoragePermissionActivity :AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_extern_permission)

        continueToGpsInit.setOnClickListener {
            if (isStoragePermissionGranted()){
                startActivity(Intent(this,GPSInitActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or
                            Intent.FLAG_ACTIVITY_NEW_TASK or
                            Intent.FLAG_ACTIVITY_CLEAR_TOP
                })
            }
            }

        }

    private fun isStoragePermissionGranted():Boolean{
        return if (Build.VERSION.SDK_INT>=23){
           return if (ActivityCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                ==PackageManager.PERMISSION_GRANTED){
                true
            }
            else{
               ActivityCompat.requestPermissions(this,
                   listOf(Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE).toTypedArray(),20001)
               (ActivityCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                       ==PackageManager.PERMISSION_GRANTED)
           }
        }
        else{
            true
        }
    }
}

