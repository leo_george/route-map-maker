package com.leo.kotlinuibackend

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Looper
import android.provider.Settings
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.*
import java.io.IOException
import java.util.*

class LocationInterface {
    private val locationRequestCode = 1000
    var latitude: Double?  = null
    var longitude: Double? = null
    var lastAddress: String? = null

    companion object{
        private var INSTANCE:LocationInterface? = null

        fun getInstance():LocationInterface{
            return  INSTANCE ?: {
                INSTANCE = LocationInterface()
                INSTANCE!!
            }()
        }
    }

     fun checkPermission(context: Context) : Boolean{
        return (ActivityCompat.checkSelfPermission(context,
            android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED
                )
    }
    fun requestPermission(activity: Activity){
        ActivityCompat.requestPermissions(activity, Array(2)
        {android.Manifest.permission.ACCESS_FINE_LOCATION },
            locationRequestCode)
    }
    fun getLastKnownLocation(context: Context,fusedLocation: FusedLocationProviderClient) {
        if(isLocationEnabled(context)){
            fusedLocation.lastLocation
                .addOnSuccessListener { location ->
                    if (location != null) {
                        latitude = location.latitude
                        longitude = location.longitude
                        val geocoder = Geocoder(context, Locale.getDefault())
                        val addresses: List<Address>
                        try {
                            addresses = geocoder.getFromLocation(
                                latitude!!,
                                longitude!!,
                                1)
                            lastAddress = addresses[0].getAddressLine((0))

                        }catch (e:IOException){
                            Toast.makeText(context,"No internet connection",Toast.LENGTH_LONG).show()
                        }
                    }
                    else{
                        requestNewLocationData(context)
                    }

                }
        }
        else{
            Toast.makeText(context, "Turn on location", Toast.LENGTH_LONG).show()
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            context.startActivity(intent)
        }
    }
    private fun isLocationEnabled(context: Context): Boolean {
        val locationManager: LocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }
    private fun requestNewLocationData(context: Context) {
        val mLocationRequest = LocationRequest.create().apply {
            fastestInterval = 10000
            interval = 10000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            smallestDisplacement = 1.0f
        }

        val mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.getMainLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation: Location = locationResult.lastLocation
            latitude = mLastLocation.latitude
            longitude = mLastLocation.longitude
        }
    }


}