package com.leo.kotlinuibackend

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.navigation_bottom.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //This will be first
        IntervalService.startService(this)

        BottomNavigationBar(
            listOf(
                nav_bottom_map to MapsFragment(),
                nav_bottom_history to LocationHistoryFragment()
            ), this
        )

    }

}
