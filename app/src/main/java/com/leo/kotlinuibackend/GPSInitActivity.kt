package com.leo.kotlinuibackend

import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.ColorFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.ImageViewCompat
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_gps_init.*
import kotlinx.coroutines.*

class GPSInitActivity :AppCompatActivity(){

   private lateinit var locationInterface: LocationInterface

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gps_init)

        locationInterface = LocationInterface.getInstance()
        val locationServices = LocationServices.getFusedLocationProviderClient(this)
        locationInterface.requestPermission(this)
        continueButton.setOnClickListener{
          if (locationInterface.checkPermission(this)) {

              locationInterface.getLastKnownLocation(this,locationServices)
              GlobalScope.launch{ isLocationSet(this@GPSInitActivity) }
          }
            else
          {
            locationInterface.requestPermission(this)
          }
          }
    }

    private suspend fun isLocationSet(context: Context){
        while (true) {
            withContext(Dispatchers.Main){
                ImageViewCompat.setImageTintList(
                    findViewById(R.id.gps_logo),
                    ColorStateList.valueOf(resources.getColor(R.color.teal))
                )
            }

            delay(2000)
            withContext(Dispatchers.Main){
                ImageViewCompat.setImageTintList(
                    findViewById(R.id.gps_logo),
                    ColorStateList.valueOf(resources.getColor(R.color.dimgrey))
                )
            }

            if (!locationInterface.lastAddress.isNullOrEmpty()) {
                ImageViewCompat.setImageTintList(
                    findViewById(R.id.gps_logo),
                    ColorStateList.valueOf(resources.getColor(R.color.teal))
                )
                withContext(Dispatchers.Main){
                    gpsStatus.text= resources.getText(R.string.gps_set)
                }
                delay(2000)
                Utils.setShowForFirstTime(context)
                startActivity(Intent(context, MainActivity::class.java))
                finish()
                break
            }
        }
    }
}