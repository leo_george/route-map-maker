package com.leo.kotlinuibackend

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Environment
import android.telephony.TelephonyManager
import android.util.Base64
import android.util.JsonReader
import android.util.Log
import android.util.Log.d
import androidx.core.app.ActivityCompat
import androidx.core.content.edit
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.security.MessageDigest


object Utils {
    private val tag = this::class.java.simpleName
    private const val IMEI_PERM = 20317
    const val FIRST_TIME="FIRST_TIME"

    fun getSHA256Hash(data :TransactionModel) : String{
        /**
         Create a base64 encoded hash(SHA-256) of  TransactionModel data class
         */
        val sha256Hash = MessageDigest.getInstance("SHA-256")
        sha256Hash.update(data.toString().toByteArray(Charsets.UTF_8))
        val hashedData = sha256Hash.digest()
        return Base64.encodeToString(hashedData, Base64.DEFAULT)


    }
    fun checkSHA256(data:TransactionModel,hash: String): Boolean{
        if(getSHA256Hash(data) == hash){
            return true
        }
        return false
    }

    private fun requestPermissions(context: Context) {
        ActivityCompat.requestPermissions(
            context as Activity,
            Array(1) { android.Manifest.permission.READ_PHONE_STATE },
            IMEI_PERM
        )
    }

    fun getIMEI(context: Context):String? {
        if(ActivityCompat.checkSelfPermission(
                context,
                android.Manifest.permission.READ_PHONE_STATE
            ) == PackageManager.PERMISSION_GRANTED)
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                (context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager).imei

            else
                (context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager).deviceId
        else{
            requestPermissions(context)
        }
        return null
    }
    fun didShowForFirstTime(context: Context):Boolean{
        return context.getSharedPreferences(StartupActivity.SHARED_PREF
            ,Context.MODE_PRIVATE).getBoolean(FIRST_TIME,false)

    }
    fun setShowForFirstTime(context: Context){
        return context.getSharedPreferences(StartupActivity.SHARED_PREF,Context.MODE_PRIVATE).edit { putBoolean(
            FIRST_TIME,true) }
    }
    fun getPhoneNo(context: Context): String?{
        return context.getSharedPreferences(StartupActivity.SHARED_PREF, Context.MODE_PRIVATE).getString(
            StartupActivity.PH_NO,"")
    }
    fun jsonToExternalStorage(json:String,context: Context){
        Log.d(tag,"inside jsontostorage")
        val blockJSONArray = JSONObject(json)
        val uid = blockJSONArray.getString("uniqueId")
        val dir:File = context.filesDir
        if (!dir.exists())
                dir.mkdirs()

        d(tag,dir.absolutePath)
        val file = File(dir,uid)
        if (file.exists()){
            val jsonArray = JSONArray(file.readText()).apply { put(blockJSONArray) }
//            d(tag,jsonArray.toString())
           file.writeText( jsonArray.toString())
        }
        else{
            val newJsonArray =JSONArray()
                val data = newJsonArray.apply {  put(json)}.toString()
//                d(tag,data)
                file.writeText(data)

            }
//        dir.listFiles()?.forEach { d(tag,it.name) }
    }
}