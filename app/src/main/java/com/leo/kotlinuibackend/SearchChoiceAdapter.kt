package com.leo.kotlinuibackend

import android.app.Activity
import android.content.Context
import android.text.Editable
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText

class SearchChoiceAdapter(private val context1: Context) : ArrayAdapter<String>(
    context1,
    R.layout.support_simple_spinner_dropdown_item,
    choices
    ), AdapterView.OnItemSelectedListener{

    companion object{
        val choices = Choices.Util.getStringList()

        fun getAdapter(context2:Context):SearchChoiceAdapter{
            return SearchChoiceAdapter(context2)
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        parent?.getChildAt(0)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        parent?.getChildAt(position)
        (context1 as Activity).findViewById<EditText>(R.id.search_box).editableText.clear()
    }

}