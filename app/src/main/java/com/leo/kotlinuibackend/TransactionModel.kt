package com.leo.kotlinuibackend

import java.sql.Date


data class TransactionModel
    (
//    val id:Int,
    val place:String ,
    val lat:String ,
    val lon:String ,
    val date: String = Date(System.currentTimeMillis()).toGMTString()
    )