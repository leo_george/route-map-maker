package com.leo.kotlinuibackend

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapsFragment : Fragment() {
    private val TAG = this.javaClass.simpleName

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (childFragmentManager.findFragmentById(R.id.mapview) as SupportMapFragment)
            .getMapAsync { googleMap ->
                val transactionViewModel =
                    ViewModelProviders.of(this).get(TransactionViewModel::class.java)
                transactionViewModel.allTransactions.removeObservers(this)
                transactionViewModel.allTransactions.observe(this, Observer { items ->
                    items?.let {
                        googleMap?.clear()
                        it.forEach { trans ->
                            val latLng = LatLng(trans.lat.toDouble(), trans.lon.toDouble())
                            googleMap?.addMarker(
                                MarkerOptions()
                                    .position(latLng)
                                    .title(trans.date)
                            )
                            googleMap?.moveCamera(CameraUpdateFactory.newLatLng(latLng))
                            googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,10f))
                        }
                    }
                })
            }
    }

}