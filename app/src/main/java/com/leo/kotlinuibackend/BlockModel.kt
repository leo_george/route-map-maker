package com.leo.kotlinuibackend

class BlockModel {
    private lateinit var uniqueId: String
    private lateinit var transactionList: MutableList<Pair<String, TransactionModel>>

    companion object {
        public const val BLOCK_SIZE = 48
        fun create(uniqueId: String): BlockModel {
            return BlockModel().apply {
                this.uniqueId = uniqueId
                this.transactionList = mutableListOf()
            }
        }
    }

    fun addTransaction(
        shaWithTransaction: Pair<String, TransactionModel>
    ): Boolean {
        return if (transactionList.size <= BLOCK_SIZE) {
            transactionList.add(shaWithTransaction)
            true
        } else {
            false
        }
    }
}