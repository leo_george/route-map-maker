package com.leo.kotlinuibackend

import android.util.Log
import com.github.nkzawa.emitter.Emitter
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import java.net.URISyntaxException

class SIO {

    companion object{
       private const val SERVER_URL = "http://192.168.1.8:3000"

        private const val ON_LOGIN = "onlogin"
        private const val LOGIN_OUTCOME = "loginoutcome"
        private const val BLOCK_DETAILS = "blockdetails"
        private const val ON_BLOCK_RECIEVED = "onBlockReceived"
        private const val ON_BRODCAST_COMMAND = "onCommandBroadcast"
        private const val EMIT_TRANS_DETAILS = "requestDetails"
        lateinit var socket: Socket

        fun getInstance(): SIO {
            try {
                socket = IO.socket(SERVER_URL)
            }
            catch (e:URISyntaxException){
                Log.d("SIO",e.reason)
            }
            return  SIO()
        }
    }
    fun doConnect(){
        socket.connect()
    }
   fun emitLoginDetails(json: String){
        socket.emit(ON_LOGIN,json)
    }
    fun listenToLoginOutcome(emitterCallback: Emitter.Listener){
        socket.on(LOGIN_OUTCOME,emitterCallback)
    }
    fun emitBlockDetails(json: String){
        socket.emit(BLOCK_DETAILS,json)
    }
    fun listenToBlockDetails(emitterCallback: Emitter.Listener){
        socket.on(ON_BLOCK_RECIEVED,emitterCallback)
    }
    fun listenToCommandReceive(emitterCallback: Emitter.Listener){
        socket.on(ON_BRODCAST_COMMAND,emitterCallback)
    }
    fun emitTransDetails(json: String){
        socket.emit(EMIT_TRANS_DETAILS,json)
    }
}