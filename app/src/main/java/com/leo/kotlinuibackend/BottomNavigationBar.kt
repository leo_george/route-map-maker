package com.leo.kotlinuibackend

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.ColorFilter
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.fragment.app.Fragment

class BottomNavigationBar(widgetFragmentPairs: List<Pair<ImageView,Fragment>>, private val context1: Context) {
    init {
        for (pairs in widgetFragmentPairs){
            pairs.first.setOnClickListener {
                replaceFragment(pairs.second)
                ImageViewCompat.setImageTintList(pairs.first,
                    ColorStateList.valueOf(context1.resources.getColor(R.color.teal)))
                widgetFragmentPairs.forEach {
                    if (it != pairs){
                        ImageViewCompat.setImageTintList(it.first,
                            ColorStateList.valueOf(context1.resources.getColor(R.color.materialgrey)))
                    }
                }
            }
        }
        replaceFragment(widgetFragmentPairs[0].second)
        ImageViewCompat.setImageTintList(widgetFragmentPairs[0].first,
            ColorStateList.valueOf(context1.resources.getColor(R.color.teal)))
    }
        private fun replaceFragment(fragment: Fragment){
            (context1 as AppCompatActivity).supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container,fragment)
                .commit()
        }
    }
