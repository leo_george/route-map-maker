package com.leo.kotlinuibackend

import android.content.Context
import android.os.AsyncTask
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlin.concurrent.thread


@Database(entities = [TransactionEntity::class],version = 1,exportSchema = false)
abstract class ChainRoomDatabase :RoomDatabase(){

    abstract fun getTransactionDao():TransactionDao

    private class ChainDBCallback(
        val scope: CoroutineScope
    ):RoomDatabase.Callback(){
        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            INSTANCE ?.let { 
                chainRoomDatabase -> scope.launch {
                val transactionDao = chainRoomDatabase.getTransactionDao()
                val transactionEntity = TransactionEntity("Kanjoor","1.455566","1.333333")
                AsyncTask.execute {
                    transactionDao.deleteAllTransactions()
                    transactionDao.newTransaction(transactionEntity)}
            }
            }
        }
    }

    companion object{
        @Volatile
        private var INSTANCE:ChainRoomDatabase ? =null

        fun getDataBaseInstance(
            context: Context,
            scope: CoroutineScope
        ):ChainRoomDatabase{
       return INSTANCE ?:
            synchronized(this){
                val instance = Room.databaseBuilder(context.applicationContext,
                    ChainRoomDatabase::class.java,"chain_db")
                    .build()
                INSTANCE=instance

                instance
            }
        }
    }
}